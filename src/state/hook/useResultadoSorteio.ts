import { useRecoilValue } from "recoil";
import { resultadoDoamigoSecreto } from "../atom";

export const useResultadoSorteio = () => {
  return useRecoilValue(resultadoDoamigoSecreto);
};
