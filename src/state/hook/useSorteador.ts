import { useSetRecoilState } from "recoil";
import { useListaDeParticipantes } from "./useListaDeParticipantes";
import { resultadoDoamigoSecreto } from "../atom";
import { realizarSorteio } from "../helpers/realizarSorteio";

export const useSorteador = () => {
  const participantes = useListaDeParticipantes();

  const setResultado = useSetRecoilState(resultadoDoamigoSecreto);

  return () => {
    const resultado = realizarSorteio(participantes);
    setResultado(resultado);
  };
};
