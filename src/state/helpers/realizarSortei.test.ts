import { realizarSorteio } from "./realizarSorteio";

describe("dado um sortei de amigo secreto", () => {
  test("cadas participante não sortei o proprio nome", () => {
    const participantes = [
      "Ana",
      "Catarina",
      "João",
      "Alisson",
      "opa",
      "Aline",
    ];

    const sorteio = realizarSorteio(participantes);

    participantes.forEach((participante) => {
      const amigoSecreto = sorteio.get(participante);
      expect(amigoSecreto).not.toEqual(participante);
    });
  });
});
