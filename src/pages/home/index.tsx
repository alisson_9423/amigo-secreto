import { Card } from "../../components/card";
import { Footer } from "../../components/footer";
import { Formulario } from "../../components/formulario";
import { ListaParticipantes } from "../../components/listaParticipantes";

export function Home() {
  return (
    <Card>
      <section>
        <Formulario />
        <ListaParticipantes />
        <Footer />
      </section>
    </Card>
  );
}
