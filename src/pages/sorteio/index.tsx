import { useState } from "react";
import { Card } from "../../components/card";
import { useListaDeParticipantes } from "../../state/hook/useListaDeParticipantes";
import { useResultadoSorteio } from "../../state/hook/useResultadoSorteio";
import { Container } from "./styles";

export function Sorteio() {
  const participantes = useListaDeParticipantes();
  const [participanteDaVez, setParticipanteDaVez] = useState("");
  const [amigoSecreto, setAmigoSecreto] = useState("");
  const resultado = useResultadoSorteio();

  const sortear = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // const amigoSecreto = resultado.get(participanteDaVez);

    if (resultado.has(participanteDaVez)) {
      setTimeout(() => {
        setAmigoSecreto("");
      }, 5000);
      setAmigoSecreto(resultado.get(participanteDaVez)!);
    }
  };

  return (
    <Card>
      <Container className="sorteio">
        <h2>Quem vai tirar o papelzinho?</h2>
        <form onSubmit={sortear}>
          <select
            required
            name="participanteDaVez"
            id="participanteDaVez"
            placeholder="Selecione o seu nome"
            value={participanteDaVez}
            onChange={(event) => setParticipanteDaVez(event.target.value)}
          >
            <option>Selecione seu nome</option>
            {participantes.map((participante) => {
              return <option key={participante}>{participante}</option>;
            })}
          </select>
          <button className="botao-sortear">Sortear</button>
        </form>
        {amigoSecreto && <p role="alert">{amigoSecreto}</p>}
        <footer className="sorteio">
          <img
            src="/imagens/aviao.png"
            className="aviao"
            alt="Um desenho de um avião de papel"
          />
        </footer>
      </Container>
    </Card>
  );
}
