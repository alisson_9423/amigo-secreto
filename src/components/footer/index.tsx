import { useNavigate } from "react-router-dom";
import { useListaDeParticipantes } from "../../state/hook/useListaDeParticipantes";
import { useSorteador } from "../../state/hook/useSorteador";
import { Container } from "./styles";

export function Footer() {
  const participantes = useListaDeParticipantes();
  const navigate = useNavigate();
  const sortear = useSorteador();

  function iniciar() {
    sortear();
    navigate("/sorteio");
  }

  return (
    <Container className="rodape-configuracoes">
      <button
        className="botao"
        onClick={iniciar}
        disabled={participantes.length < 3}
      >
        Iniciar brincadeira!
      </button>
    </Container>
  );
}
