import { useRef, useState } from "react";
import { Container } from "./styles";
import { useAdicionarParticipante } from "../../state/hook/useAdicionarParticipante";
import { useMensagemdeErro } from "../../state/hook/useMensagemDeErro";

export function Formulario() {
  const [nome, setNome] = useState("");
  const inputRef = useRef<HTMLInputElement>(null);
  const adicionarNaLista = useAdicionarParticipante();
  const mensagemDeErro = useMensagemdeErro();

  const adicionarParticipante = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    adicionarNaLista(nome);
    setNome("");

    if (inputRef.current) {
      inputRef.current?.focus();
    }
  };

  return (
    <Container onSubmit={adicionarParticipante}>
      <div className="grupo-input-btn">
        <input
          type="text"
          placeholder="Insira os nomes dos participantes"
          value={nome}
          onChange={(e) => setNome(e.target.value)}
          ref={inputRef}
        />
        <button disabled={!nome}>Adicionar</button>

        {mensagemDeErro && (
          <p className="alerta erro" role="alert">
            {mensagemDeErro}
          </p>
        )}
      </div>
    </Container>
  );
}
