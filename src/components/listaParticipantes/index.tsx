import { useListaDeParticipantes } from "../../state/hook/useListaDeParticipantes";

export function ListaParticipantes() {
  const participantes: string[] = useListaDeParticipantes();

  return (
    <ul>
      {participantes.map((participante) => {
        return <li key={participante}>{participante}</li>;
      })}
    </ul>
  );
}
